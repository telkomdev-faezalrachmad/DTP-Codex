<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class request extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
    }

	function index() {
		$data = array(
					'flashdata' => $this->session->flashdata(),
				);
		$this->load->view('request/form', $data);
	}

	function request() {
		extract($_POST);
		if ( $unitName && $persons && $projectName && $projectDetails ) {
			$dataRequest 	= array(
								'unitName' => $unitName,
								'persons' => $persons,
								'projectName' => $projectName,
								'projectDetails' => $projectDetails,
								'projectNeeds' => $projectNeeds,
							);
			$insert_request	= $this->request_model->insert_request( $dataRequest );
			if( $insert_request ) {
				$this->session->set_flashdata('message', 'Data berhasil disubmit.');
				$this->session->set_flashdata('status', 'success');
				redirect(site_url( 'form/request' ));
			}else {
				$this->session->set_flashdata('message', 'Maaf, terjadi kesalahan saat submit!');
				$this->session->set_flashdata('status', 'danger');
				redirect(site_url( 'form/request' ));
			}
		}else{
			$this->session->set_flashdata('message', 'Maaf, data anda kosong atau belum terisi semua.');
				$this->session->set_flashdata('status', 'danger');
				redirect(site_url( 'form/request' ));
		}
	}
}