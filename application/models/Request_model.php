<?php
	Class Request_model extends CI_Model {
		private $request_table		= "tb_request";

		function get_all() {
			$data = $this->db->get($this->request_table);
			return $data;
		}

		function insert_request( $data ) {
			$query = $this->db->insert( $this->request_table, $data );
			if ($query) {
				# code...
				return TRUE;
			} else{
				return FALSE;
			}
		}
	}