<!-- START SCRIPTS -->
    <!-- START PLUGINS -->
    <script type="text/javascript" src="js/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="js/plugins/jquery/jquery-ui.min.js"></script>
    <script type="text/javascript" src="js/plugins/bootstrap/bootstrap.min.js"></script>            
    <!-- END PLUGINS -->
    
    <!-- THIS PAGE PLUGINS -->
    <script type='text/javascript' src='<?php echo base_url() ?>assets/js/plugins/icheck/icheck.min.js'></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
    
    <script type="text/javascript" src="<?php echo base_url() ?>assets/js/plugins/bootstrap/bootstrap-datepicker.js"></script>                
    <script type="text/javascript" src="<?php echo base_url() ?>assets/js/plugins/bootstrap/bootstrap-file-input.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/js/plugins/bootstrap/bootstrap-select.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/js/plugins/tagsinput/jquery.tagsinput.min.js"></script>
    <!-- END THIS PAGE PLUGINS -->       
    <!--script type="text/javascript" src="<?php echo base_url() ?>assets/js/plugins/datatables/jquery.dataTables.min.js"></script-->
    <!-- START TEMPLATE -->
    <!--script type="text/javascript" src="<?php echo base_url() ?>assets/js/settings.js"></script-->
    
    <script type="text/javascript" src="<?php echo base_url() ?>assets/js/plugins.js"></script>        
    <script type="text/javascript" src="<?php echo base_url() ?>assets/js/actions.js"></script>        
    <!-- END TEMPLATE -->
<!-- END SCRIPTS -->